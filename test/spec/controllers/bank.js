'use strict';

describe('Controller AmdBankWalletsCtrl', function() {

	// load the controller's module
	beforeEach(module('ngMaterialDashboardBank'));

	var AmdBankWalletsCtrl;
	var scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function($controller, $rootScope, _$usr_) {
		scope = $rootScope.$new();
		AmdBankWalletsCtrl = $controller('AmdBankGateCtrl', {
			$scope : scope,
			$usr : _$usr_
			// place here mocked dependencies
		});
	}));

	it('should be defined', function() {
		expect(angular.isDefined(AmdBankWalletsCtrl)).toBe(true);
	});
});
