/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardBank', [ //
    'ngMaterialDashboard',//
    'seen-bank'
]);

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardBank')
/**
 * Icon configuration.
 */
.config(function() {
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardBank')
	/**
	 * 
	 */
	.config(function ($routeProvider) {
	    $routeProvider //
		    .when('/bank/gates', {
			controller: 'AmdBankGatesCtrl',
			templateUrl: 'views/amd-bank-gates.html',
			navigate: true,
			protect: function ($rootScope) {
			    return !$rootScope.app.user.tenant_owner;
			},
			name: 'Bank gates',
			groups: ['bank'],
			icon: 'attach_money',
			hidden: '!app.user.tenant_owner'
		    }) //
		    .when('/bank/gates/new', {
			controller: 'AmdBankGateNewCtrl',
			templateUrl: 'views/amd-bank-gate-new.html',
			//navigate : true,
			name: 'New bank gate',
			groups: ['bank'],
			icon: 'add',
			protect: function ($rootScope) {
			    return !$rootScope.app.user.tenant_owner;
			}
		    }) //
		    .when('/bank/gates/:gateId', {
			controller: 'AmdBankGateCtrl',
			templateUrl: 'views/amd-bank-gate.html',
			protect: function ($rootScope) {
			    return !$rootScope.app.user.tenant_owner;
			}
		    }) //
		    .when('/bank/banks', {
			controller: 'AmdBanksCtrl',
			controllerAs: 'ctrl',
			templateUrl: 'views/amd-bank-banks.html',
			navigate: true,
			name: 'Bank engines',
			icon: 'attach_money',
			groups: ['bank'],
			hidden: '!app.user.tenant_owner',
			protect: function ($rootScope) {
			    return !$rootScope.app.user.tenant_owner;
			}
		    }) //
//		    .when('/bank/banks/:bankType', {
//			controller: 'AmdBankCtrl',
//			templateUrl: 'views/amd-bank-bank.html',
//			protect: function ($rootScope) {
//			    return !$rootScope.app.user.tenant_owner;
//			}
//		    })
		    .when('/bank/receipts', {
			controller: 'AmdBankReceiptsCtrl',
			templateUrl: 'views/amd-bank-receipts.html',
			navigate: true,
			name: 'Recipts',
			icon: 'receipt',
			groups: ['bank'],
			protect: function ($rootScope) {
			    return !$rootScope.app.user.tenant_owner;
			}
		    })
		    .when('/bank/receipts/:id', {
			templateUrl: 'views/amd-bank-receipt.html',
			controller: 'AmdBankReceiptCtrl',
			controllerAs: 'ctrl'
		    });//;
	});

'use strict';

angular.module('ngMaterialDashboardBank')

/**
 * @ngdoc controller
 * @name AmdBankGates
 * @description Manages bank backends
 * 
 */
.controller('AmdBankGateCtrl', function ($scope, $bank, $location, $routeParams, $navigator, $translate) {

    var ctrl = {
            state: 'relax',
            edit: false
    }
    /**
     * درخواست مورد نظر را از سیستم حذف می‌کند.
     * 
     * @param request
     * @returns
     */
    function remove() {
        confirm($translate.instant('The bank gate will be deleted.'))//
        .then(function () {
            $scope.gate.delete()
            .then(function () {
                $location.path('/bank/gates');
            }, function () {
                alert($translate.instant('Fail to delete bank gate'));
            });
        });
    }

    function load() {
        if (ctrl.state !== 'relax') {
            return;
        }
        ctrl.state = 'working';
        return $bank.getBackend($routeParams.gateId)//
        .then(function (gate) {
            $scope.gate = gate;
            return $scope.gate;
        })
        .finally(function () {
            ctrl.state = 'relax';
        });
    }

    function update() {
        if (ctrl.state !== 'relax') {
            return;
        }
        ctrl.state = 'working';
        return $scope.gate.update()//
        .then(function (gate) {
            $scope.gate = gate;
            ctrl.edit = false;
            return $scope.gate;
        })
        .finally(function () {
            ctrl.state = 'relax';
        });
    }

    $scope.remove = remove;
    $scope.load = load;
    $scope.update = update;
    $scope.ctrl = ctrl;
    load();

});

'use strict';

angular.module('ngMaterialDashboardBank')

/**
 * @ngdoc controller
 * @name AmdBankGates
 * @description Manages bank backends
 * 
 */
.controller('AmdBankGateNewCtrl', function($scope, $bank, QueryParameter, $navigator, $translate) {

    /**
     * Load banks
     * 
     * @returns
     */
    function loadBanks(){
    	return $bank.getEngines()//
    	.then(function(banks){
    	    $scope.banks = banks;
    	});
    }

    function loadBankProperties(bank){
    	return $bank.getEngine(bank.type)//
    	.then(function(property){
    	    $scope.properties = property;
    	});
    }

    function newGate(bank, data){
    	$scope.creatingNewGate = true;
    	data.type = bank.type;
    	return $bank.putBackend(data)//
    	.then(function(){
    	    toast($translate.instant('New bank gate is created successfully'));
    	    $navigator.openPage('/bank/gates');
    	}, function(){
    	    alert($translate.instant('Fail to create new bank gate'));
    	})//
    	.finally(function(){
    	    $scope.creatingNewGate = false;
    	});
    }

// $scope.$watch('_bank', function(value){
//	
// return loadBankProperties(value);
// });
    /*
     * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
     */
    $scope.items = [];

    $scope.loadBanks =loadBanks;
    $scope._userValus = {};
    $scope.newGate = newGate;
    $scope.loadBankProperties = loadBankProperties;
});

'use strict';

angular.module('ngMaterialDashboardBank')

/**
 * @ngdoc controller
 * @name AmdBankGates
 * @description Manages bank backends
 * 
 */
.controller('AmdBankGatesCtrl', function ($scope, $bank, QueryParameter, $navigator, $translate) {

    var paginatorParameter = new QueryParameter();
    paginatorParameter.setOrder('id', 'd');
    var requests = null;
    var ctrl = {
            status: 'relax',
            items: []
    };

    /**
     * لود کردن داده‌های صفحه بعد
     * 
     * @returns
     */
    function nextPage() {
        if (ctrl.status === 'working') {
            return;
        }
        if (requests && !requests.hasMore()) {
            return;
        }
        if (requests) {
            paginatorParameter.setPage(requests.next());
        }
        // start state (device list)
        ctrl.status = 'working';
        $bank.getBackends(paginatorParameter)//
        .then(function (items) {
            requests = items;
            ctrl.items = ctrl.items.concat(requests.items);
            ctrl.status = 'relax';
        }, function () {
            ctrl.status = 'fail';
        });
    }


    /**
     * درخواست مورد نظر را از سیستم حذف می‌کند.
     * 
     * @param request
     * @returns
     */
    function remove(pobject) {
        confirm($translate.instant('The bank gate will be deleted.'))//
        .then(function () {
            pobject.delete()//
            .then(function () {
                var index = ctrl.items.indexOf(pobject);
                if (index > -1) {
                    ctrl.items.splice(index, 1);
                }
            });
        });
    }

    /**
     * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
     * 
     * @returns
     */
    function reload() {
        requests = null;
        ctrl.items = [];
        nextPage();
    }


    function newGate(bank, data) {
        $scope.creatingNewGate = true;
        data.type = bank.type;
        return $bank.putBackend(data)//
        .then(function () {
            $navigator.openPage('/bank/gates');
        }, function () {
            alert($translate.instant('Fail to create new bank gate'));
        })//
        .finally(function () {
            $scope.creatingNewGate = false;
        });
    }
    /*
     * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
     */

    $scope.nextPage = nextPage;
    $scope.remove = remove;
    $scope.newGate = newGate;
    $scope.reload = reload;
    $scope.ctrl = ctrl;


    // Pagination
    $scope.paginatorParameter = paginatorParameter;
    $scope.sortKeys = [
        'id',
        'creation_dtime'
        ];
    $scope.moreActions = [{
        title: 'New bank gate',
        icon: 'add',
        action: function () {
            $navigator.openPage('/bank/gates/new');
        }
    }];

});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialDashboardBank')


/**
 * @ngdoc controller
 * @name AmdBankReceiptCtrl
 * @description manage a receipt
 * 
 */
.controller('AmdBankReceiptCtrl', function ($routeParams, $bank, $window) {

    /**
     * Sets a receipt in the scope
     * 
     * @param receipt
     */
    this.setReceipt = function (receipt) {
        this.receipt = receipt;
        // TODO: set page title
    };

    /**
     * Get the receipt id
     * @returns {string} id of the recept
     */
    this.getReceiptId = function () {
        return ($routeParams.id || null);
    };

    /**
     * Loads receipt data
     */
    this.loadReceipt = function () {
        if (this.loading) {
            return;
        }
        this.loading = true;
        var ctrl = this;
        //TODO: maso,2019: Check for graphql
        return $bank.getReceipt(this.getReceiptId())//
        .then(function (receipt) {
            ctrl.setReceipt(receipt);
            return $bank.getBackend(receipt.backend_id);
        }, function (error) {
            ctrl.error = error;
        })//
        .then(function (gate) {
            ctrl.gate = gate;
        })//
        .finally (function () {
            ctrl.loading = false;
        });
    };

    /**
     * Cancel page
     */
    this.cancel = function () {
        $window.history.back();
    };

    this.loadReceipt();
});

'use strict';

angular.module('ngMaterialDashboardBank')

/**
 * @ngdoc controller
 * @name AmdBankReceiptsCtrl
 * @description Manages bank backends
 * 
 */
.controller('AmdBankReceiptsCtrl', function($scope, $bank, QueryParameter , $translate) {

	var paginatorParameter = new QueryParameter();
	var requests = null;
	var ctrl = {
		items: []
	};


	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.loadingReceipts || (requests && !requests.hasMore())) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		ctrl.loadingReceipts = true;
		$bank.getReceipts(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		}, function() {
			alert($translate.instant('Failed to load receipts.'));
		})
		.finally(function(){
			ctrl.loadingReceipts = false;
		});
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload() {
		requests = null;
		ctrl.items = [];
		nextPage();
	}


	$scope.ctrl = ctrl;
	$scope.nextPage = nextPage;
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.sortKeys = [ 'id', 'title', 'amount', 'due_dtime', 'creation_dtime' ];
	$scope.sortKeysTitles = [ 'Id', 'Title', 'Amount', 'Due time', 'creation time'];

});



    
  
  

'use strict';

angular.module('ngMaterialDashboardBank')

/**
 * @ngdoc controller
 * @name AmdBankGates
 * @description Manages bank backends
 * 
 */
.controller('AmdBanksCtrl', function ($scope, $bank, $controller) {

    // Extends with ItemsController
    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
        $scope: $scope
    }));

    /*
     * Overried the function
     */
    this.getModelSchema = function () {
        return $bank.engineSchema();
    };

    // get engines
    this.getModels = function (parameterQuery) {
        return $bank.getEngines(parameterQuery);
    };

    // get a engine
    this.getModel = function (id) {
        return $bank.getEngine(id);
    };
    
    // delete a engine
    this.deleteModel = function (id) {
        return $bank.deleteEngine(id);
    };

    this.init({
        eventType: '/bank/engines'
    });
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardBank')
/**
 * دریچه‌های محاوره‌ای
 */
.run(function($navigator) {
	$navigator
	.newGroup({
		id: 'bank',
		title: 'Bank management',
		description: 'A module of dashboard to manage bank.',
		icon: 'attach_money',
		priority: 5,
                hidden: '!app.user.tenant_owner'
	});
});
angular.module('ngMaterialDashboardBank').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-bank-bank.html',
    ""
  );


  $templateCache.put('views/amd-bank-banks.html',
    "<div mb-preloading=ctrl.loading layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex>  <md-list flex> <md-list-item ng-repeat=\"bank in ctrl.items track by bank.type\" class=md-3-line> <img ng-src=\"{{bank.url|| ('images/bank/' + bank.symbol + '.svg')}}\" class=\"md-avatar\"> <div class=md-list-item-text layout=column> <h3>{{bank.title}}</h3> <h4 ng-if=\"bank.name || bank.version\">{{bank.name}}-{{bank.version}}</h4> <p>{{bank.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-if=\"ctrl.state === 'ideal' && (!ctrl.items || ctrl.items.length == 0)\"> <h2 translate=\"\">No item found</h2> </div> </md-content> </div>"
  );


  $templateCache.put('views/amd-bank-gate-new.html',
    "<md-content layout=column layout-padding flex> <div layout=column ng-cloak> <h3 translate=\"\">Select a bank from the list:</h3> <div layout=column> <md-select required md-no-asterisk=false placeholder=Bank ng-model=_bank ng-change=loadBankProperties(_bank) md-on-open=loadBanks() style=\"min-width: 200px\"> <md-option ng-value=bank ng-repeat=\"bank in banks.items\"> <span translate=\"\">{{bank.title}}</span> </md-option> </md-select> </div> </div> <span flex=10></span> <div mb-preloading=creatingNewGate ng-init=\"_userValues = {}\" layout=column ng-show=properties> <h3 translate=\"\">{{properties.title}}</h3> <p translate=\"\">{{properties.description}}</p> <md-input-container ng-repeat=\"property in properties.children\"> <div ng-if=\"property.name !== 'symbol'\"> <label>{{property.title}}</label> <input ng-model=_userValus[property.name]> </div> <div ng-if=\"property.name === 'symbol'\"> <wb-ui-setting-image title=Symbol ng-model=\"_userValus['symbol']\"> </wb-ui-setting-image> </div> </md-input-container> <div> <md-button class=md-raised ng-href=bank/gates> <span translate>Cancel</span> </md-button> <md-button class=\"md-primary md-raised\" ng-click=\"newGate(_bank, _userValus)\"> <span translate>Save</span> </md-button> </div> </div> </md-content>"
  );


  $templateCache.put('views/amd-bank-gate.html',
    "<md-content class=md-padding layout-padding flex> <mb-titled-block ng-if=!ctrl.edit mb-progress=ctrl.loading mb-title=\"{{'Gate'| translate}}\" layout=column> <div layout=row layout-xs=column layout-align-xs=\"center center\" layout-padding> <img width=100px height=100px style=\"border-radius: 10px\" ng-src=\"{{gate.symbol|| gate.url|| ('images/bank/' + gate.symbol + '.svg')}}\"> <table> <tr> <td translate=\"\">ID </td> <td>: {{gate.id}}</td> </tr> <tr> <td translate=\"\">Title </td> <td>: {{gate.title}}</td> </tr> <tr> <td translate=\"\">Description </td> <td>: {{gate.description}}</td> </tr> </table> </div> <div layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=remove()> <span translate=\"\">Delete</span> </md-button> <md-button class=\"md-raised md-primary\" ng-click=\"ctrl.edit = true\"> <span translate=\"\">Edit</span> </md-button> </div> </mb-titled-block> <mb-titled-block ng-if=ctrl.edit mb-progress=working mb-title=\"{{'Edit gate'| translate}}\" layout=column> <form name=myForm ng-action=update(config) layout=column flex> <md-input-container> <label tranlate=\"\">Title </label> <input ng-model=gate.title name=title required> <div ng-messages=myForm.title.$error> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <md-input-container> <label tranlate=\"\">Description</label> <input ng-model=gate.description> </md-input-container> <wb-ui-setting-image title=\"{{'Image'| translate}}\" wb-ui-setting-clear-button=true wb-ui-setting-preview=true ng-model=gate.symbol> </wb-ui-setting-image> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=\"ctrl.edit = false;load()\"> <span translate=\"\">Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" ng-disabled=myForm.$invalid ng-click=update()> <span translate=\"\">Update</span> </md-button> </div> </form> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-bank-gates.html',
    "<div ng-if=\"ctrl.status !== 'working'\" mb-infinate-scroll=nextPage() layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"gate in ctrl.items\" class=md-3-line ng-href=\"{{'bank/gates/' + gate.id}}\"> <img ng-src=\"{{gate.url || gate.symbol || ('images/bank/' + gate.symbol + '.svg')}}\" class=\"md-avatar\"> <div class=md-list-item-text layout=column> <h3>{{gate.title}}</h3> <h4>{{gate.home}}</h4> <p>{{gate.description}}</p> </div> <md-button class=md-icon-button ng-click=remove(gate) ng-aria=\"Remove gate\"> <wb-icon>delete</wb-icon> </md-button> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=row flex=100 layout-align=\"center center\" ng-if=\"ctrl.status === 'working'\"> <md-progress-circular md-diameter=96> Loading ... </md-progress-circular> </div> </md-content> </div>"
  );


  $templateCache.put('views/amd-bank-receipt.html',
    "<md-content class=md-padding layout-padding flex> <mb-titled-block mb-progress=ctrl.loading mb-title=\"{{'Payment receipt'| translate}}\" layout=column> <div layout=row layout-xs=column layout-align-xs=\"center center\" layout-padding> <table> <tr> <td translate=\"\">ID </td> <td>: {{ctrl.receipt.id}}</td> </tr> <tr> <td translate=\"\">Title </td> <td>: {{ctrl.receipt.title}}</td> </tr> <tr> <td translate=\"\">Amount </td> <td>: {{ctrl.receipt.amount}}</td> </tr> <tr> <td translate=\"\">Description </td> <td>: {{ctrl.receipt.description}}</td> </tr> <tr> <td translate=\"\">Backend </td> <td>: {{ctrl.gate.title}}</td> </tr> <tr> <td translate=\"\">Status </td> <td>: <span ng-if=!ctrl.loading style=\"color: red\" translate=\"\">{{ctrl.receipt.payRef ? 'payed' : 'not payed'}}</span></td> </tr> </table> </div>  <div ng-if=!ctrl.receipt.payRef layout=row> <span flex></span> <md-button ng-disabled=ctrl.receipt.payRef ng-show=!ctrl.receipt.payRef class=\"md-raised md-primary\" ng-href={{ctrl.receipt.callURL}}> <span translate=\"\">Complete pay</span> </md-button> <md-button class=\"md-raised md-accent\" ng-click=ctrl.cancel()> <span translate=\"\">Cancel</span> </md-button> </div> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-bank-receipts.html',
    "<div layout=column mb-preloading=ctrl.loadingReceipts flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex ng-if=\"!ctrl.loadingReceipts && ctrl.items.length\"> <md-list-item ng-repeat=\"item in ctrl.items\" class=md-3-line> <wb-icon>{{item.status == 'payed' ? 'check_circle' : 'attach_money'}}</wb-icon> <div class=md-list-item-text layout=column> <h3>{{item.title}}</h3> <h4>{{'Amount' | translate}}: {{item.amount}}</h4> <p>{{item.creation_dtime | mbDate:'jYYYY-jMM-jDD'}}</p> </div> <md-divider md-inset></md-divider> </md-list-item> </md-list> <div layout=column ng-if=\"!ctrl.loadingReceipts && !ctrl.items.length\"> <h3 style=\"text-align: center\" translate=\"\">Nothing found.</h3> </div> </md-content> </div>"
  );

}]);
